use figment::{Error, Figment, providers::{Format, Toml, Yaml}};
use log::{debug, error, info};
use serde::{Deserialize, Serialize};

#[derive(Debug)]
pub enum RouterError {
    RouteNotFound,
    UrlNotFound,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Routes {
    pub name: String,
    pub token: Token,
    pub routes: Vec<Route>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Token {
    pub jwt_provider: String,
    pub jwt_refresh: String,
}

impl Default for Routes {
    fn default() -> Self {
        Self {
            name: String::from("Default"),
            token: Token {
                jwt_provider: String::from("http://default.com/api/v1/users/login"),
                jwt_refresh: String::from("http://default.com/api/v1/users/refresh-token"),
            },
            routes: Vec::new(),
        }
    }
}

impl Routes {
    pub fn load() -> Result<Routes, Error> {
        info!("Loading routes");
        let settings: Routes = Figment::new()
            .join(Toml::file("conf/routes.toml"))
            .join(Yaml::file("conf/routes.yaml"))
            .extract()?;
        Ok(settings)
    }

    pub fn find_route(&self, path: &str) -> Result<&Route, RouterError> {
        info!("Finding route: {}", path);
        for route in &self.routes {
            debug!("Route: {}", route.path);
            if route.path == path {
                return Ok(route);
            }
        }
        error!("Route not found for pattern: {}", path);
        Err(RouterError::RouteNotFound)
    }
}

#[derive(Debug, Deserialize, Serialize, Clone, PartialEq)]
pub struct Route {
    pub path: String,
    pub methods: Vec<String>,
    pub upstream: Option<String>,
    pub upstream_path: Option<String>,
    pub upstream_headers: Option<Vec<String>>,
    pub file_path: Option<String>,
    pub token_refresh: Option<String>,
    pub invalid_cert: Option<bool>,
}

impl Route {
    pub fn has_header(&self, key: &str) -> bool {

        let headers = match &self.upstream_headers {
            Some(headers) => headers,
            None => return false,
        };
        for header in headers.iter() {
            if header.to_lowercase() == key.to_lowercase() {
                return true;
            }
        }
        false
    }

    pub fn url(&self) -> Option<String> {
        info!("Getting the upstream URL");
        let upstream = match &self.upstream {
            Some(upstream) => upstream,
            None => "",
        };
        let upstream_path = match &self.upstream_path {
            Some(upstream_path) => upstream_path,
            None => "",
        };
        let url = format!("{}{}", upstream, upstream_path);
        match url.as_str() {
            "" => {
                error!("Error: Getting the upstream URL, URL not found");
                None
            },
            _ => {
                debug!("Upstream URL: {}", url);
                Some(url)
            },
        }
    }

    pub fn endpoint(&self, tail: Option<String>) -> Result<String, RouterError> {
        info!("Getting the end point URL with tail: {:?}", tail);
        if self.url().is_none() {
            log::error!("Error: Getting the end point, URL not found");
            return Err(RouterError::UrlNotFound);
        }
        let endpoint = match tail {
            Some(tail) => format!("{}/{}", self.url().unwrap(), tail),
            None => self.url().unwrap(),
        };
        debug!("Endpoint URL: {}", endpoint);
        Ok(endpoint)
    }
}

