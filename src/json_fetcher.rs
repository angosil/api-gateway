use std::fs;

use actix_web::http::header::HeaderMap;
use actix_web::http::Method;
use actix_web::web;
use log::{debug, error, info};
use reqwest::{Client, StatusCode};
use serde_json::Value;

#[derive(Debug, PartialEq)]
pub enum JsonFetcherError {
    FailedToReadFile,
    FailedToParseJson,
    FailedToMakeRequest,
    RouteDefinitionError,
}

#[derive(Debug)]
pub enum JsonFetcher {
    File(FileJsonFetcher),
    Request(RequestJsonFetcher),
}

impl JsonFetcher {
    pub async fn fetch_json(&self) -> Result<(StatusCode, Value), JsonFetcherError> {
        match self {
            JsonFetcher::File(file_fetcher) => file_fetcher.fetch_json().await,
            JsonFetcher::Request(request_fetcher) => request_fetcher.fetch_json().await,
        }
    }
}

#[derive(Debug)]
pub struct FileJsonFetcher {
    file_path: String,
}

impl FileJsonFetcher {
    pub fn new(file_path: String) -> Self {
        Self { file_path }
    }

    async fn fetch_json(&self) -> Result<(StatusCode, Value), JsonFetcherError> {
        info!("Reading file: {}", &self.file_path);
        let file_content = fs::read_to_string(&self.file_path).map_err(|_| {
            error!("Failed to read file: {}", &self.file_path);
            JsonFetcherError::FailedToReadFile
        })?;
        let json_content: Value = serde_json::from_str(&file_content).map_err(|_| {
            error!("Failed to parse json from file: {}", &self.file_path);
            JsonFetcherError::FailedToParseJson
        })?;
        Ok((StatusCode::OK, json_content))
    }
}

#[derive(Debug)]
pub struct RequestJsonFetcher {
    url: String,
    method: Method,
    headers: HeaderMap,
    data: Option<Value>,
    invalid_cert: Option<bool>,
}

impl RequestJsonFetcher {
    pub fn new(url: String, method: Method, headers: HeaderMap, data: Option<Value>, invalid_cert: Option<bool>) -> Self {
        Self { url, method, headers, data, invalid_cert }
    }
    async fn fetch_json(&self) -> Result<(StatusCode, Value), JsonFetcherError> {
        info!("Making request to: {}", &self.url);

        debug!("Creating a new request client");
        let client = Client::builder()
            .danger_accept_invalid_certs(self.invalid_cert.unwrap_or(true))
            .build()
            .unwrap();

        debug!("Making request to destination URL");
        let mut client_request = client.request(self.method.clone(), self.url.clone());

        debug!("Add headers to the request");
        for (key, value) in &self.headers {
            debug!("Request header: {}={:?}", key, value);
            client_request = client_request.header(key.clone(), value.clone());
        }

        debug!("Add data to the request");
        if let Some(data) = &self.data {
            log::debug!("Data: {:?}", data);
            client_request = client_request.json(&data);
        }

        let response = client_request.send().await.map_err(|_| {
            error!("Failed to make request to: {}", &self.url);
            JsonFetcherError::FailedToMakeRequest
        })?;

        let status_code = response.status();
        let json_content: Value = response.json().await.map_err(|_| {
            error!("Failed to parse json from response");
            JsonFetcherError::FailedToParseJson
        })?;
        Ok((status_code, json_content))
    }
}

#[cfg(test)]
mod tests {
    use serde_json::Value;

    use crate::test_setup::setup;

    use super::*;

    #[tokio::test]
    async fn test_file_json_fetcher() {
        setup();
        let fetcher = FileJsonFetcher::new("tests/fixtures/test.json".to_string());
        debug!("File Fetcher: {:?}", fetcher);
        let json_content = fetcher.fetch_json().await;

        debug!("JSON Content: {:?}", json_content);

        assert!(json_content.is_ok());
        assert_eq!(json_content.unwrap(), serde_json::json!({"data": "Hello, World!"}));
    }
}