use std::sync::Once;

use env_logger;

static INIT: Once = Once::new();


pub fn setup() {
    INIT.call_once(|| {
        env_logger::init();
    });
}