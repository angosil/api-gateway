//! This is an API gateway that acts as a reverse proxy for a set of services. It handles authentication
//! and routing requests to the appropriate service.
//!
//! The API gateway listens on port 8098 and provides an endpoint for users to login and receive a
//! JWT token. The token can then be used to make authenticated requests to the services.
//!
//! ## Usage
//!
//! Start the server by running `cargo run` in the project directory. The API gateway can then be accessed
//! at http://127.0.0.1:8098.
//!
//! ## Configuration
//!
//! The API gateway is configured using a TOML configuration file located at `conf/routes.toml`. The
//! following options are available:
//!
//! - `port` (number): The port to listen on. Default is `8098`.
//! - `jwt_provider_url` (string): The URL of the JWT provider service. Default is `http://localhost:8080/auth`.
//!
//! ## Dependencies
//!
//! This API gateway depends on the following crates:
//!
//! - `actix-web`: For building the web server.
//! - `env_logger`: For logging.
//! - `log`: For logging.
//! - `reqwest`: For making HTTP requests to the JWT provider service.
//! - `serde`: For serializing and deserializing data.
//! - `serde_json`: For working with JSON data.
//! - `conf`: For loading configuration files.

extern crate log;

use std::fs::File;
use std::io::Read;

use actix_cors::Cors;
use actix_web::{App, get, HttpRequest, HttpResponse, HttpServer, route, web};
use clap::Parser;
use reqwest::Client;
use serde_json::{json, Value};

use proxy_request::ProxyRequest;
use settings::RouterError;

mod settings;

mod proxy_request;
mod json_fetcher;
mod test_setup;

/// Handler for the root endpoint.
#[get("/api/v1")]
async fn welcome() -> HttpResponse {
    log::info!("Welcome to API gateway");
    let body = json!({
        "message": "Welcome to API gateway",
        "version": "v1"
    });
    log::debug!("Response body: {}", body);
    HttpResponse::Ok().json(body)
}

#[route(
"/api/{version}/{service}/{tail}*",
    method = "GET",
    method = "POST",
    method = "PUT",
    method = "DELETE"
)]
async fn proxy(req: HttpRequest, data: Option<web::Json<Value>>) -> HttpResponse {
    log::info!("Proxy request received");

    // Get params from http request url
    log::debug!("Getting params from http request url");
    let version = req.match_info().get("version").unwrap_or("");
    let service = req.match_info().get("service");
    let tail = match req.match_info().get("tail") {
        Some(tail) if !tail.is_empty() => Some(tail.to_string()),
        _ => None,
    };
    let head = req.head();
    let method = req.method();

    let proxy_request = ProxyRequest::new(
        version.to_string(),
        service.map(|s| s.to_string()),
        tail,
        head.headers.clone(),
        method.clone(),
        data.map(|json| json.into_inner()),
    );

    // Get the conf file
    log::debug!("Getting routes");
    let routes = match settings::Routes::load() {
        Ok(routes) => routes,
        Err(error) => {
            log::error!("Error: {}", error);
            return HttpResponse::InternalServerError().json(error.to_string());
        }
    };

    match proxy_request.execute(routes).await {
        Ok((status, json_content)) => {
            let status_code = actix_web::http::StatusCode::from_u16(status.as_u16())
                .unwrap_or(actix_web::http::StatusCode::INTERNAL_SERVER_ERROR);
            HttpResponse::build(status_code).json(json_content)
        },
        Err(_) => HttpResponse::InternalServerError().body("Internal server error"),
    }
}

#[derive(Parser, Debug)]
#[command()]
struct Args {
    #[arg(short, long, default_value = "0.0.0.0")]
    server_ip: String,
    #[arg(short, long, default_value = "8098")]
    port: u16,
    #[arg(long, default_value = "info")]
    rust_log: String,
}

fn log_setup(rust_level: String) {
    //! Set the log level. A format the logger to show the path of the function.
    log::info!("Setting up the logger");
    std::env::set_var("RUST_LOG", rust_level);
    env_logger::init();
}

/// The entry point of the program.
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Start of the program
    println!("*** Starting API gateway ***");

    // Get the command line arguments
    let args = Args::parse();

    // Setup the logger
    log_setup(args.rust_log);

    // Start the server
    println!("*** Starting the server at {}:{} ***", args.server_ip, args.port);
    HttpServer::new(|| {
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_method()
            .allow_any_header()
            .max_age(3600);
        App::new()
            .wrap(cors)
            .service(welcome)
            .service(proxy)
    })
        .bind(format!("{}:{}", args.server_ip, args.port))?
        .run()
        .await
}
