use std::sync::Arc;

use actix_web::{http::header::HeaderMap, http::Method};
// src/proxy_request
use log::{debug, error, info};
use serde_json::Value;
use reqwest::StatusCode;

use crate::json_fetcher::{FileJsonFetcher, JsonFetcher, RequestJsonFetcher};
use crate::settings::{Route, Routes};

#[derive(Debug, PartialEq)]
pub enum RoutingError {
    RouteNotFound,
    FailedToReadFile,
    FailedToParseJson,
    FailedToMakeRequest,
    RouteDefinitionError,
}

pub struct ProxyRequest {
    version: String,
    service: Option<String>,
    tail: Option<String>,
    head: HeaderMap,
    method: Method,
    data: Option<Value>,
}

impl ProxyRequest {
    pub fn new(version: String, service: Option<String>, tail: Option<String>, head: HeaderMap, method: Method, data: Option<Value>) -> Self {
        info!("Creating new proxy request");
        Self {
            version,
            service,
            tail,
            head,
            method,
            data,
        }
    }

    fn build_route_pattern(&self) -> String {
        info!("Building route pattern");
        let version = &self.version;
        let service = self.service.as_ref().map_or("", |s| s.as_str());
        let route_pattern = match service {
            "" => format!("{}", version),
            _ => format!("{}/{}", version, service),
        };
        route_pattern
    }

    pub fn find_route<'a>(&self, routes: &'a Routes) -> Result<&'a Route, RoutingError> {
        info!("Finding route");
        let route_pattern = self.build_route_pattern();
        return match routes.find_route(&route_pattern) {
            Ok(route) => Ok(route),
            Err(_) => {
                error!("Route not found for pattern: {}", route_pattern);
                Err(RoutingError::RouteNotFound)
            }
        };
    }

    fn get_json_fetcher(&self, route: &Route) -> Result<JsonFetcher, RoutingError> {
        info!("Getting json fetcher");
        if let Some(file_path) = &route.file_path {
            Ok(JsonFetcher::File(FileJsonFetcher::new(file_path.clone())))
        } else if let Some(_) = route.url() {
            let end_point = route.endpoint(self.tail.clone());

            debug!("Copying headers from the original request to the client request...");
            let mut headers = HeaderMap::new();
            for (key, value) in self.head.iter() {
                debug!("Request header: {}={:?}", key, value);
                if route.has_header(&key.to_string()) {
                    debug!("Allowed header: {}={:?}", key, value);
                    headers.append(key.clone(), value.clone())
                }
            }

            Ok(JsonFetcher::Request(RequestJsonFetcher::new(end_point.unwrap(), self.method.clone(), headers, self.data.clone(), route.invalid_cert)))
        } else {
            error!("Route definition error: file_path and url are not defined");
            Err(RoutingError::RouteNotFound)
        }
    }

    pub async fn execute(&self, routes: Routes) -> Result<(StatusCode, Value), RoutingError> {
        info!("Executing route");
        let route_pattern = self.build_route_pattern();
        debug!("Route pattern: {}", route_pattern);
        match self.find_route(&routes) {
            Ok(route) => {
                debug!("Route found: {}", route.path);
                let json_fetcher = self.get_json_fetcher(route)?;
                let (status_code, json_content) = json_fetcher.fetch_json().await.map_err(|_| RoutingError::FailedToMakeRequest)?;
                Ok((status_code, json_content))
            }
            Err(_) => {
                error!("Route not found for pattern: {}", route_pattern);
                Err(RoutingError::RouteNotFound)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use actix_web::http::header::HeaderMap;
    use actix_web::http::Method;
    use env_logger;

    use crate::settings::Routes;
    use crate::test_setup::setup;

    use super::*;

    fn setup_proxy_request(version: Option<String>, service: Option<String>, tail: Option<String>) -> ProxyRequest {
        let version = version.unwrap_or("v1".to_string());
        let head = HeaderMap::new();
        let method = Method::GET;

        ProxyRequest::new(version, service, tail, head, method, None)
    }

    #[test]
    fn test_proxy_request_new() {
        let service = Some("service".to_string());
        let tail = Some("tail".to_string());
        let version = Some("v1".to_string());

        let proxy_request = setup_proxy_request(None, service.clone(), tail.clone());

        assert_eq!(proxy_request.version, version.unwrap());
        assert_eq!(proxy_request.service, service);
        assert_eq!(proxy_request.tail, tail);
        assert_eq!(proxy_request.head.len(), 0);
        assert_eq!(proxy_request.method, Method::GET);
    }

    #[test]
    fn test_build_route_pattern_full() {
        let service = Some("service".to_string());

        let proxy_request = setup_proxy_request(None, service.clone(), None);

        assert_eq!(proxy_request.build_route_pattern(), "v1/service");
    }

    #[test]
    fn test_build_route_pattern_version_only() {
        let proxy_request = setup_proxy_request(None, None, None);

        assert_eq!(proxy_request.build_route_pattern(), "v1");
    }


    #[test]
    fn test_find_route() {
        let service = Some("service".to_string());
        let tail = Some("tail".to_string());

        let proxy_request = setup_proxy_request(None, service, tail);

        let mut routes = Routes::default();
        routes.routes.push(Route {
            path: "v1/service".to_string(),
            methods: vec!["GET".to_string()],
            upstream: None,
            upstream_path: None,
            upstream_headers: None,
            file_path: None,
            token_refresh: None,
            invalid_cert: None,
        });

        let result = proxy_request.find_route(&routes);

        assert!(result.is_ok());
        assert_eq!(result.unwrap().path, "v1/service");
    }

    #[test]
    fn test_find_route_not_found() {
        let service = Some("service".to_string());
        let tail = Some("tail".to_string());

        let proxy_request = setup_proxy_request(None, service, tail);

        let mut routes = Routes::default();
        routes.routes.push(Route {
            path: "v1/another_service".to_string(),
            methods: vec!["GET".to_string()],
            upstream: None,
            upstream_path: None,
            upstream_headers: None,
            file_path: None,
            token_refresh: None,
            invalid_cert: None,
        });

        let result = proxy_request.find_route(&routes);

        assert!(result.is_err());
        assert_eq!(result.unwrap_err(), RoutingError::RouteNotFound);
    }

    #[test]
    fn test_get_json_fetcher_file() {
        let service = Some("service".to_string());
        let tail = Some("tail".to_string());

        let proxy_request = setup_proxy_request(None, service, tail);

        let route = Route {
            path: "v1/service".to_string(),
            methods: vec!["GET".to_string()],
            upstream: None,
            upstream_path: None,
            upstream_headers: None,
            file_path: Some("tests/fixtures/test.json".to_string()),
            token_refresh: None,
            invalid_cert: None,
        };

        let json_fetcher = proxy_request.get_json_fetcher(&route);

        assert!(json_fetcher.is_ok());
        assert!(matches!(json_fetcher.unwrap(), JsonFetcher::File(_)));
    }

    #[test]
    fn test_get_json_fetcher_request() {
        let service = Some("service".to_string());
        let tail = Some("tail".to_string());

        let proxy_request = setup_proxy_request(None, service, tail);

        let route = Route {
            path: "v1/service".to_string(),
            methods: vec!["GET".to_string()],
            upstream: Some("http://localhost:8080".to_string()),
            upstream_path: None,
            upstream_headers: None,
            file_path: None,
            token_refresh: None,
            invalid_cert: Some(true),
        };

        let json_fetcher = proxy_request.get_json_fetcher(&route);

        assert!(json_fetcher.is_ok());
        assert!(matches!(json_fetcher.unwrap(), JsonFetcher::Request(_)));
    }

    #[test]
    fn test_get_json_fetcher_route_definition_error() {
        let service = Some("service".to_string());
        let tail = Some("tail".to_string());

        let proxy_request = setup_proxy_request(None, service, tail);

        let route = Route {
            path: "v1/service".to_string(),
            methods: vec!["GET".to_string()],
            upstream: None,
            upstream_path: None,
            upstream_headers: None,
            file_path: None,
            token_refresh: None,
            invalid_cert: None,
        };

        let json_fetcher = proxy_request.get_json_fetcher(&route);

        assert!(json_fetcher.is_err());
        assert_eq!(json_fetcher.unwrap_err(), RoutingError::RouteNotFound);
    }

    #[tokio::test]
    async fn test_execute() {
        let service = Some("service".to_string());

        let proxy_request = setup_proxy_request(None, service, None);

        let mut routes = Routes::default();
        routes.routes.push(Route {
            path: "v1/service".to_string(),
            methods: vec!["GET".to_string()],
            upstream: None,
            upstream_path: None,
            upstream_headers: None,
            file_path: Some("tests/fixtures/test.json".to_string()),
            token_refresh: None,
            invalid_cert: None,
        });

        let result = proxy_request.execute(routes).await;

        debug!("Execute result: {:?}", result);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), serde_json::json!({"data": "Hello, World!"}));
    }
}